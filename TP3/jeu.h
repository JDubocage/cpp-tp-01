#ifndef JEU_H
#define JEU_H

#include <iostream>
#include <string>
#include "joueur.h"
#include "grille.h"
#include "grillemorpion.h"
#include "grillepuissance4.h"

class Jeu
{
public:
    // Constructeur
    Jeu(Grille* _grille);
    //Destructeur
    ~Jeu();

private:
    // Attributs
    Grille* grille;
    Joueur joueurUn;
    Joueur joueurDeux;
    Joueur joueurCourant;
    bool partieTerminee = false;

    // Logique m�tier
    void initPartie();
    void boucleDeJeu();
    void joueTour();
    int valideSaisie(const std::string saisie, const int borne) const;
    int valideNombre(const std::string choix) const;
    int valideBornes(const int valChoix, const int borne) const;
    bool checkCaseVide(const int x, const int y) const;
    void feliciteGagnant(const std::string pseudoGagnant) const;
    void finTour();
};

#endif // JEU_H
