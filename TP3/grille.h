#ifndef GRILLE_H
#define GRILLE_H

#include <iostream>
#include <array>
#include <vector>
#include <stdbool.h>
#include "joueur.h"

class Grille {
public:
    // Constructeur
    Grille(const int _nbLignes, const int _nbColonnes);

    // Attributs
    const int NB_LIGNES;
    const int NB_COLONNES;
    std::vector<std::vector<int>> grille;

    // Logique métier
    void deposerJeton(const int idJoueur, const int x, const int y);
    bool caseVide(const int x, const int y) const;
    bool grillePleine() const;
    void Affichage() const;
    bool victoireJoueur(const int idJoueur) const;

    // Constantes de classe
    static const int CASE_VIDE = 0;
    enum Diagonale { DIAGONALE_ASCENDANTE, DIAGONALE_DESCENDANTE };

private:
    // Logique métier
    void initGrille();
    char getCaseChar(const int x, const int y) const;
    virtual bool ligneComplete(const int idJoueur, const int x) const = 0;
    virtual bool colonneComplete(const int idJoueur, const int y) const = 0;
    virtual bool diagonaleComplete(const int idJoueur, const int diag) const = 0;
};

#endif // GRILLE_H
