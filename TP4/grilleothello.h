#ifndef GRILLEOTHELLO_H
#define GRILLEOTHELLO_H
#include "grille.h"
#include "joueur.h"


class GrilleOthello : public Grille
{
public:
    GrilleOthello();
    void initGrille();
    bool peutJouer(const int idJoueur, const int idAdversaire) const;
    bool jouerCoup(const int idJoueur, const int idAdversaire, const int x, const int y);
    int joueurGagnant(const int idJoueurUn, const int idJoueurDeux) const;
    int getScore(const int idJoueur) const;
private:
    bool coupJouable(const int idJoueur, const int idAdversaire, const int x, const int y) const;
    bool estJouable(const int idJoueur) const;
    void retourneJeton(const int idJoueur, const int idAdversaire, const int x, const int y);
};

static const int NB_LIGNES_OTHELLO = 8;
static const int NB_COLONNES_OTHELLO = 8;
#endif // GRILLEOTHELLO_H
