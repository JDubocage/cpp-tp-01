#include "jeufabrique.h"

JeuFabrique::JeuFabrique(){}

// Patron Fabrique qui renvoie le bon jeu selon le choix de l'utilisateur
std::unique_ptr<Jeu> JeuFabrique::choixJeu(const int choix) const {
    switch (choix) {
    case MORPION:
        return std::unique_ptr<Jeu>(new JeuMorpion);
    case PUISSANCE_4:
        return std::unique_ptr<Jeu>(new JeuPuissance4);
    case OTHELLO:
        return std::unique_ptr<Jeu>(new JeuOthello);
    default:
        return NULL;
    }
}
