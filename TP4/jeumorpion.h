#ifndef JEUMORPION_H
#define JEUMORPION_H

#include "jeu.h"
#include "grillemorpion.h"

class JeuMorpion : public Jeu
{
public:
    JeuMorpion();
private:
    GrilleMorpion grille;

    void boucleDeJeu();
    void joueTour();
};

#endif // JEUMORPION_H
