 #ifndef JOUEUR_H
#define JOUEUR_H

#include <string>

class Joueur
{
public:
    // Constructeur
    Joueur();
    Joueur(int _id, std::string _pseudo);
    // Getters
    inline int getId() const{ return id; }
    inline std::string getPseudo() const{ return pseudo; }

    // Constantes de classe
    static const int ID_JOUEUR_1 = 1;
    static const int ID_JOUEUR_2 = 2;

private:
    // Attributs
    int id;
    std::string pseudo;
};

#endif // JOUEUR_H
