#include "jeu.h"

// Constructeur
Jeu::Jeu(){}

// Initialise une partie de Morpion ou Puissance4
// (Cr?ation des joueurs + lancement de la boucle de jeu)
void Jeu::initPartie() {

    // Cr?ation des deux joueurs
    std::string pseudoJ1, pseudoJ2;

    std::cout << "Veuillez entrer le pseudo du 1er joueur : " << std::endl;
    std::cin >> pseudoJ1;
    this->joueurUn = Joueur(Joueur::ID_JOUEUR_1, pseudoJ1);

    std::cout << "Veuillez entrer le pseudo du 2eme joueur : " << std::endl;
    std::cin >> pseudoJ2;
    this->joueurDeux = Joueur(Joueur::ID_JOUEUR_2, pseudoJ2);

    std::cout << std::endl;

    // C'est le joueur 1 qui commence
    this->joueurCourant = joueurUn;

    // Lancement de la boucle de jeu
    this->partieTerminee = false;
    this->boucleDeJeu();
}

// M?thode qui passe le tour au joueur suivant
void Jeu::finTour() {
    if (joueurCourant.getId() == joueurUn.getId()) {
        joueurCourant = joueurDeux;
    }
    else joueurCourant = joueurUn;
}

// Affiche un message de felicitation sur la console
void Jeu::feliciteGagnant(const std::string pseudoGagnant) const {
    std::cout << "Felicitations " << pseudoGagnant << " !" << std::endl;
    std::cout << "Tu es le vainqueur de la partie" << std::endl;
}

// V?rifie si une saisie peut etre convertie en entier
bool Jeu::verifieType(const std::string saisie) const {
    try {
        std::stoi(saisie);
    }
    catch (std::invalid_argument const& ex) {
        std::cout << "Saisie incorrecte, veuillez entrer un entier !" << std::endl;
        return false;
    }
    return true;
}

// Convertit une chaine de caracteres en entier en elevant le decallage (1ere colonne/ligne = index 0 du tableau)
int Jeu::convertitChaine(const std::string chaine) const {
    return std::stoi(chaine) - 1;
}
