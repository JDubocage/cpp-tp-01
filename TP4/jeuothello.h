#ifndef JEUOTHELLO_H
#define JEUOTHELLO_H

#include "jeu.h"
#include "grilleothello.h"

class JeuOthello : public Jeu
{
public:
    JeuOthello();
private:
    GrilleOthello grille;

    void boucleDeJeu();
    void joueTour();
    int getIdAdversaire() const;
    void afficheScores() const;
};

#endif // JEUOTHELLO_H
