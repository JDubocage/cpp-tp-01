#ifndef JEU_H
#define JEU_H

#include <iostream>
#include <string>
#include "joueur.h"
#include "grille.h"

class Jeu
{
public:
    // Constructeur
    Jeu();
    void initPartie();

protected:
    // Attributs
    bool partieTerminee;
    Joueur joueurUn;
    Joueur joueurDeux;
    Joueur joueurCourant;

    // Logique m�tier
    void feliciteGagnant(const std::string pseudoGagnant) const;
    void finTour();
    bool verifieType(const std::string saisie) const;
    int convertitChaine(const std::string chaine) const;
    virtual void boucleDeJeu() = 0;
    virtual void joueTour() = 0;
};

#endif // JEU_H
