#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H

#include <stdbool.h>
#include <array>
#include <iostream>
#include "grille.h"

class GrillePuissance4 : public Grille
{
public: 
    // Construteur
	GrillePuissance4();
    // Logique m�tier
    void initGrille();
    bool jouerCoup(const int idJoueur, const int colonne);
    bool victoireJoueur(const int idJoueur) const;

private:
    // Logique m�tier
    int premiereCaseVide(const int colonne) const;
    bool ligneComplete(const int idJoueur, const int x) const;
    bool colonneComplete(const int idJoueur, const int y) const;
    bool diagonaleComplete(const int idJoueur, const int diag) const;

    // Constantes de classe
    static const int NB_LIGNES_PUISSANCE4 = 4;
    static const int NB_COLONNES_PUISSANCE4 = 7;
    static const int NB_JETONS_VICTOIRE = 4;
};

#endif // GRILLEPUISSANCE4_H
