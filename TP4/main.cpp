#include <QCoreApplication>
#include <iostream>
#include <stdbool.h>
#include "jeu.h"
#include "jeufabrique.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    JeuFabrique fabrique = JeuFabrique();
    int choix;
    bool saisieCorrecte = false;

    // Affichage du menu
    std::cout << "********** Selection du Jeu **********" << std::endl;
    std::cout << "1. Morpion" << std::endl;
    std::cout << "2. Puissance 4" << std::endl;
    std::cout << "3. Othello" << std::endl;

    // Boucle de choix tant que la saisie n'est pas correcte
    do {
        std::cout << "Veuillez faire votre choix : ";
        std::cin >> choix;
        saisieCorrecte = true;

        if (choix < 1 || choix > 3){
            std::cout << "Saisie incorrecte" << std::endl;
        }
        else saisieCorrecte = true;
    }
    while (!saisieCorrecte);

    std::cout << std::endl;

    // Traitement du choix
    std::unique_ptr<Jeu> jeu = fabrique.choixJeu(choix);

    if (jeu != NULL){
        // Lancement de la partie
        jeu->initPartie();
    }

}

