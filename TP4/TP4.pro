QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    grille.cpp \
    grillemorpion.cpp \
    grilleothello.cpp \
    grillepuissance4.cpp \
    jeu.cpp \
    jeuothello.cpp \
    joueur.cpp \
    main.cpp \
    jeufabrique.cpp \
    jeumorpion.cpp \
    jeupuissance4.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    grille.h \
    grillemorpion.h \
    grilleothello.h \
    grillepuissance4.h \
    jeu.h \
    jeuothello.h \
    joueur.h \
    jeufabrique.h \
    jeumorpion.h \
    jeupuissance4.h
