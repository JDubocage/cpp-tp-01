#include "grilleothello.h"

GrilleOthello::GrilleOthello():Grille(NB_LIGNES_OTHELLO, NB_COLONNES_OTHELLO){}

void GrilleOthello::initGrille(){
    // On initialise toutes les cases de la grille à la valeur de CASE_GRILLE
    grille.assign(NB_LIGNES, std::vector<int>(NB_COLONNES, 0));
    // Puis on place les 4 premiers jetons nécessaires au démarrage de la partie (2 pour chaque joueur)
    grille[3][3] = Joueur::ID_JOUEUR_1;
    grille[4][4] = Joueur::ID_JOUEUR_1;
    grille[3][4] = Joueur::ID_JOUEUR_2;
    grille[4][3] = Joueur::ID_JOUEUR_2;
}

bool GrilleOthello::jouerCoup(const int idJoueur, const int idAdversaire, const int x, const int y) {
    // On vérifie que le coup est bien compris entre les bornes du jeu
    if (y < 0 || y > NB_LIGNES) {
        std::cout << "Saisie incorrecte (le choix de ligne doit etre compris entre 1 et " << NB_LIGNES << ")" << std::endl;
        return false;
    }

    if (x < 0 || x > NB_COLONNES) {
        std::cout << "Saisie incorrecte (le choix de colonne doit etre compris entre 1 et " << NB_COLONNES << ")" << std::endl;
        return false;
    }

    // On vérifie que la case est vide
    if (!this->caseVide(x, y)){
        std::cout << "La case choisie n'est pas vide" << std::endl;
        return false;
    }

    if(!coupJouable(idJoueur, idAdversaire, x, y)){
        std::cout << "Le coup n'est pas autorisé : il ne permet pas de retourner de pions adverses" << std::endl;
        return false;
    }

    this->placerJeton(idJoueur, x, y);
    retourneJeton(idJoueur, idAdversaire, x, y);
    return true;
}

void GrilleOthello::retourneJeton(const int idJoueur, const int idAdversaire, const int x, const int y){
    // on creer une liste de coordonées des jetons à retourner
    std::vector<std::vector<int>> jetonsARetouner;

    // Création du tableau pour vérifier les 8 positions voisines
    int positionsVoisinnes[8][2] = {{-1, -1}, {-1, 0}, {-1, 1}, // 3 positions au dessus
                                      {0, -1}, {0, 1}, // 2 positions sur la même abscice
                                      {1, -1}, {1, 0}, {1, 1}}; // 3 positions en dessous

    // Pour chaque direction autour du point courant
    for(auto coordonnee : positionsVoisinnes){

        // On enregistre les coordonées du prochain point dans la direction
        int curr_y = y + coordonnee[0];
        int curr_x = x + coordonnee[1];

         // Si on est en dehors de la grille on passe à la direction suivante
        if(y+coordonnee[0] > 7 || y+coordonnee[0] < 0 || x+coordonnee[1] > 7 || x+coordonnee[1] < 0){
            continue;
        }

        // on enregistre ce qu'il y a dans la grille aux nouvelles coordonnées
        char jetonPosition = grille[curr_y][curr_x];
        bool retournerDirection = false;

        // Si le premier jeton est un adverse
        if(jetonPosition == idAdversaire){
            // Tant que le jeton courant est un jeton adverse on continue d'avancer dans cette direction
            while(jetonPosition == idAdversaire){
                curr_y += coordonnee[0];
                curr_x += coordonnee[1];

                // Si on est en dehors de la grille on passe à la direction suivante
                if(curr_y > 7 || curr_y < 0 || curr_x > 7 || curr_x < 0)
                    break;
                jetonPosition = grille[curr_y][curr_x];
            }

            // Si l'on retrouve un jeton du joueur courant dans la direction
            if(jetonPosition == idJoueur){
                retournerDirection = true;
            }

            // Si la direction courante est à retourner
            if(retournerDirection){
                curr_y = y + coordonnee[0];
                curr_x = x + coordonnee[1];
                jetonPosition = grille[curr_y][curr_x];

                // Tant que le le jeton courant est un adverse
                while(jetonPosition == idAdversaire){
                    // on ajoute les coordonées du jeton dans la liste
                    std::vector<int> jeton = {curr_y, curr_x};
                    jetonsARetouner.push_back(jeton);
                    // on avance dans la direction
                    curr_y += coordonnee[0];
                    curr_x += coordonnee[1];

                    // on enregistre ce qu'il y a dans la grille aux nouvelles coordonées
                    jetonPosition = grille[curr_y][curr_x];
                }

            }
        }
        for(auto pos : jetonsARetouner){
            grille[pos[0]][pos[1]] = idJoueur;
        }
    }
}

// Permet de déterminer si un joueur donné peut jouer ou non (selon les regles) sur la grille
bool GrilleOthello::peutJouer(const int idJoueur, const int idAdversaire) const {
    for(int i = 0; i < 8; ++i){
        for(int j = 0; j < 8; ++j){
            // On verifie que la case est vide
            if(grille[i][j] == CASE_VIDE){
                // on verifie que le coup est jouable
                if(coupJouable(idJoueur, idAdversaire, i, j)){
                    return true;
                }
            }
        }
    }
    return false;
}

// Renvoie l'id du joueur gagnant ou -1 si personne n'a gagné
int GrilleOthello::joueurGagnant(const int idJoueurUn, const int idJoueurDeux) const {
    // On vérifie qu'aucun de des joueurs ne peut plus jouer
    if (peutJouer(idJoueurUn, idJoueurDeux) || peutJouer(idJoueurDeux, idJoueurUn)){
        return -1;
    }

    // On calcule les scores pour déterminer le gagnant
    if (getScore(idJoueurUn) > getScore(idJoueurDeux)){
        return idJoueurUn;
    }
    return idJoueurDeux;
}

bool GrilleOthello::coupJouable(const int idJoueur, const int idAdversaire, const int x, const int y) const {

    // Création du tableau pour vérifier les 8 positions voisines
    int positionsVoisinnes[8][2] = {{-1, -1}, {-1, 0}, {-1, 1},
                                      {0, -1}, {0, 1},
                                      {1, -1}, {1, 0}, {1, 1}};

    // Pour chaque direction autour du point courant
    for(auto coordonnee : positionsVoisinnes){

        // Si on est en dehors de la grille on passe au suivant
        if(y+coordonnee[0] > 7 || y+coordonnee[0] < 0 || x+coordonnee[1] > 7 || x+coordonnee[1] < 0){
            continue;
        }
        int jeton = grille[y+coordonnee[0]][x+coordonnee[1]]; // On enregistre le premier jeton dans cette direction

        // Si le premier jeton est un adverse
        if(jeton == idAdversaire){
            // on enregistre les coordonées en tant que coordonées du jeton courant
            int curr_y = y + coordonnee[0];
            int curr_x = x + coordonnee[1];

            // On continue dans la même direction tant que le jeton courant est un jeton adverse
            while(jeton == idAdversaire){
                curr_y += coordonnee[0];
                curr_x += coordonnee[1];

                // On verifie si les nouvelles coordonées sont toujours dans la grille
                if(curr_y > 7 || curr_y < 0 || curr_x > 7 || curr_y < 0){
                    break;
                }
                // Enregistre le nouveau jeton en tant que jeton courant
                jeton = grille[curr_y][curr_x];
            }
            // Si le dernier jeton trouver était un un jeton du joueur courant on renvoie true.
            if(jeton == idJoueur)
                return true;
        }
    }
    return false;
}

// Permet de calculer le score d'un joueur
int GrilleOthello::getScore(int idJoueur) const {
    int total = 0;
    for(int y = 0; y < 8; ++y)
        for(int x = 0; x < 8; ++x)
            if(grille[x][y] == idJoueur)
                total += 1;

    return total;
}

