#include "grillemorpion.h"

GrilleMorpion::GrilleMorpion(): Grille(NB_LIGNES_MORPION, NB_COLONNES_MORPION) {}

// Initialisation de toute les cases de la grille � la valeur de CASE_VIDE
void GrilleMorpion::initGrille() {
    grille.assign(NB_LIGNES, std::vector<int>(NB_COLONNES, 0));
}

bool GrilleMorpion::jouerCoup(const int idJoueur, const int x, const int y){
    //On v�rifie que le coup est bien compris entre les bornes du jeu
    if (y < 0 || y > NB_LIGNES) {
        std::cout << "Saisie incorrecte (le choix de ligne doit etre compris entre 1 et " << NB_LIGNES << ")" << std::endl;
        return false;
    }

    if (x < 0 || x > NB_COLONNES) {
        std::cout << "Saisie incorrecte (le choix de colonne doit etre compris entre 1 et " << NB_COLONNES << ")" << std::endl;
        return false;
    }

    // On v�rifie que la case est vide
    if (!this->caseVide(x, y)){
        std::cout << "La case choisie n'est pas vide" << std::endl;
        return false;
    }

    // Le coup est valide, on peut placer le jeton
    this->placerJeton(idJoueur, x, y);
    return true;
}

// V�rifie si un joueur donn�e a rempli une des conditions de victoire
bool GrilleMorpion::victoireJoueur(const int idJoueur) const {
    // On v�rifie si le joueur a une ligne compl�te
    for (int i = 0; i < NB_LIGNES; i++) {
        if (this->ligneComplete(idJoueur, i)) {
            return true;
        }
    }
    // On v�rifie si le joueur � une colonne compl�te
    for (int j = 0; j < NB_COLONNES; j++) {
        if (this->colonneComplete(idJoueur, j)) {
            return true;
        }
    }
    // On v�rifie si le joueur a une diagonale complete
    if (this->diagonaleComplete(idJoueur, DIAGONALE_ASCENDANTE) || this->diagonaleComplete(idJoueur, DIAGONALE_DESCENDANTE)) {
        return true;
    }

    return false;
}

// V�rifie si une ligne donn�e de la grille est compl�te
bool GrilleMorpion::ligneComplete(const int idJoueur, const int x) const {
    for (int i = 0; i < NB_COLONNES; i++){
        if (this->grille[x][i] != idJoueur){
            return false;
        }
    }
    return true;
}

// V�rifie si une colonne donn�e de la grille est compl�te
bool GrilleMorpion::colonneComplete(const int idJoueur, const int y) const {
    for (int i = 0; i < NB_LIGNES; i++){
        if (this->grille[i][y] != idJoueur){
            return false;
        }
    }
    return true;
}

// V�rifie si une des deux diagonales de la grille est compl�te
bool GrilleMorpion::diagonaleComplete(const int idJoueur, const int diag) const {
    if (diag == DIAGONALE_DESCENDANTE) {
        for (int i = 0, j = 0; i < NB_COLONNES && j < NB_LIGNES; i++, j++) {
            if (this->grille[i][j] != idJoueur) {
                return false;
            }
        }
        return true;
    }
    else if (diag == DIAGONALE_ASCENDANTE) {
        for (int i = NB_COLONNES - 1, j = 0; i > 0 && j < NB_LIGNES; i--, j++) {
            if (this->grille[i][j] != idJoueur) {
                return false;
            }
        }
        return true;
    }
    return false;
}

