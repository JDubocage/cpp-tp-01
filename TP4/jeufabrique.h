#ifndef JEUFABRIQUE_H
#define JEUFABRIQUE_H

#include <string>
#include <memory>
#include "jeu.h"
#include "jeumorpion.h"
#include "jeupuissance4.h"
#include "jeuothello.h"

enum Mode { MORPION = 1, PUISSANCE_4 = 2, OTHELLO = 3 };

class JeuFabrique
{
public:
    JeuFabrique();
    std::unique_ptr<Jeu> choixJeu(const int choix) const ;
};

#endif // JEUFABRIQUE_H
