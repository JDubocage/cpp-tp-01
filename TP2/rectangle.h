#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "point.h"

class Rectangle 
{
private:
    // Définition des propriétées
    int longeur;
    int largeur;
    POINT coinSupGauche;
    
public:
    // Constructeur par défaut
    Rectangle();
    // Constructeur avec paramètres
    Rectangle(int _longeur, int _largeur);
    // Getters
    inline int getLongeur() { return longeur; }
    inline int getLargeur() { return largeur; }
    inline POINT getCoinSupGauche() { return coinSupGauche; }
    // Setters
    inline void setLongeur(int _longeur){ longeur = _longeur; }
    inline void setLargeur(int _largeur){ largeur = _largeur; }
    inline void setCoinSupGauche(POINT _coinSupGauche){ coinSupGauche = _coinSupGauche; }

    // Methodes utilitaires
    inline int getPerimetre() const{ return 2*longeur + 2*largeur; }
    inline int getSurface() const { return longeur*largeur; }
    void Afficher() const;
};

#endif // RECTANGLE_H