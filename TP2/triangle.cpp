#include <iostream>
#include "triangle.h"

// Constructeur par défaut
Triangle::Triangle() : a(), b(), c() {}
// Constructeur avec paramètres
Triangle::Triangle(POINT _a, POINT _b, POINT _c) : a(_a), b(_b), c(_c) {}

// Renvoie les 3 longueurs du triangle sous forme de tableau [AB,BC,AC]
std::array<float, 3> Triangle::getLongueurs() const{
    std::array<float, 3> longueurs = {
        a.calculeDistance(b),
        b.calculeDistance(c),
        a.calculeDistance(c)
    };
    return longueurs;
}

float Triangle::getBase() const {
    // On calcule et récupère les longueurs
    std::array<float, 3> longeurs = getLongueurs();
    float longueurAB = longeurs[0];
    float longueurBC = longeurs[1];
    float longueurAC = longeurs[2];

    // Puis on renvoie la plus grande valeur
    if (longueurAB > longueurBC && longueurAB > longueurAC)
    {
        return longueurAB;
    }
    else if (longueurBC > longueurAC)
    {
        return longueurBC;
    }
    else
    {
        return longueurAC;
    }
}

// Calcul de l'aire d'un triangle quelconque (Formule de Héron)
float Triangle::calculeAire() const {
    // On calcule et récupère les longueurs
    std::array<float, 3> longeurs = getLongueurs();
    float longueurAB = longeurs[0];
    float longueurBC = longeurs[1];
    float longueurAC = longeurs[2];

    // On calcule le demi-perimètre
    float p = (longueurAB + longueurBC + longueurAC) / 2;

    // Puis on peut calculer et renvoyer l'aire
    return sqrt(static_cast<double>(p) * (static_cast<double>(p) - longueurBC) * (static_cast<double>(p) - longueurAC) * (static_cast<double>(p) - longueurAB) );
}

// Calcul de le hauteur d'un triangle quelconque (en utilisant l'aire et la formule de Héron)
float Triangle::calculeHauteur() const {
    return (2 * calculeAire()) / getBase();
}

// Détermine si le triangle est isocèle (c'est à dire s'il a au moins deux coté de même longeur)
bool Triangle::estIsocele() const {
    // On calcule et récupère les longueurs
    std::array<float, 3> longeurs = getLongueurs();
    if (longeurs[0] == longeurs[1] || longeurs[0] == longeurs[2] || longeurs[1] == longeurs[2]) {
        return true;
    }
    return false;
}

// Détermine si le triangle est équilatéral (c'est à dire si ces 3 côtés ont la même longeur)
bool Triangle::estEquilateral() const {
    // On calcule et récupère les longueurs
    std::array<float, 3> longeurs = getLongueurs();
    return (longeurs[0] == longeurs[1] && longeurs[1] == longeurs[2]);
}

// Détermine si un triangle est rectangle à l'aide du Théorème de Pythagore
bool Triangle::estRectangle() const {
    if (estEquilateral()) {
        return false;
    }
    // On calcule et récupère les longueurs et la base
    float base = getBase();
    std::array<float, 2> cotes;
    int i = 0;
    for (float longueur : getLongueurs()) {
        if (longueur != base) {
            cotes[i] = longueur;
            i++;
        }
    }
    // Si la longeur au carré de l'hypothénuse est égale à la somme des carrés des 2 autres longeur
    // -> le triangle est rectangle
    return (pow(base, 2) == (pow(cotes[0], 2) + pow(cotes[1], 2)));
}

void Triangle::Afficher() const {
    std::array<float, 3> longeurs = getLongueurs();
    std::cout << "Triangle : " << std::endl;
    std::cout << "Longeurs : [AB = " << longeurs[0] << ", BC = " << longeurs[1] << ", AC = " << longeurs[2] << "]" << std::endl;
    std::cout << "Hauteur : " << calculeHauteur() << std::endl;
    std::cout << "Aire : " << calculeAire() << std::endl;
    std::cout << "Isocele ? : " << estIsocele() << std::endl;
    std::cout << "Equilatéral ? : " << estEquilateral() << std::endl;
    std::cout << "Rectangle ? : " << estRectangle() << std::endl;
}


