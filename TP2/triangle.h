#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <array>
#include "point.h"

class Triangle
{
private:
    // Définition des propriétées
    POINT a;
    POINT b;
    POINT c;

public:
    // Constructeur par défaut
    Triangle();
    // Constructeur avec paramètres
    Triangle(POINT _a, POINT _b, POINT _c);
    // Getters
    inline POINT getPointA() { return a; }
    inline POINT getPointB() { return b; }
    inline POINT getPointC() { return c; }
    // Setters 
    inline void setPointA(POINT _a){a = _a;}
    inline void setPointB(POINT _b){b = _b;}
    inline void setPointC(POINT _c){c = _c;}
    // Méthodes utilitaires
    std::array<float, 3> getLongueurs() const;
    float getBase() const;
    float calculeAire() const;
    float calculeHauteur() const;
    bool estIsocele() const;
    bool estEquilateral() const;
    bool estRectangle() const;
    void Afficher() const;
    
};

#endif // TRIANGLE_H