#ifndef CERCLE_H
#define CERCLE_H
#define _USE_MATH_DEFINES

#include "point.h"
#include <math.h>

class Cercle
{
private:
    // Définition des propriétées   
    POINT centre;
    int diametre;

public:
    // Constructeur par défaut
    Cercle();
    // Constructeur avec paramètres
    Cercle(POINT _centre, int _diametre);
    // Getters
    inline POINT getCentre() { return centre; }
    inline int getDiametre() { return diametre; }
    // Setters
    inline void setCentre(POINT _centre){ centre = _centre; }
    inline void setDiametre(int _diametre){ diametre = _diametre; }
    // Méthodes utilitaires
    inline float getRayon() const { return diametre/2.0; }
    float getPerimetre() const;
    float getSurface() const;
    bool estSurCercle(const POINT point) const;
    bool estInterieurDuCercle(const POINT point) const;
    void Afficher() const;
};

#endif // CERCLE_H