#include "program.h"

int main()
{
    srand(time(0));
    DevineNombre();
}

void Salutation()
{
    string nom;
    cout << "Donne moi ton nom et prénom :" << endl;
    getline(cin, nom);
    cout << "Bonjour " << nom << endl;
}

void DevineNombre()
{
    int nombre = rand() % 1001;
    int essai;
    int nbEssais = 0;
    cout << "Un nombre entre 0 et 1000 a été généré, essayez de le deviner :" << endl;

    do
    {
        cin >> essai;
        ++nbEssais;

        if (nombre > essai)
        {
            cout << "Le nombre est plus grand !" << endl;
        }
        else if (nombre < essai)
        {
            cout << "Le nombre est plus petit !" << endl;
        }
        else
        {
            cout << "Bravo !" << endl;
            cout << "Vous avez deviné le nombre en " << nbEssais << " essais" << endl;
        }
    }
    while (essai != nombre);
}