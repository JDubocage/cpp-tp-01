#include "tennis.h"

const string J1 = "premier";
const string J2 = "second";

void DetermineScore(int echangesJ1, int echangesJ2){
    // Un des joueurs a 40 et marque l'echange sans egalite
    if (echangesJ1 > 3 && echangesJ2 < 3){
       AfficheScore(J1, echangesJ1, echangesJ2);
    }
    else if (echangesJ2 > 3 && echangesJ1 < 3){
        AfficheScore(J2, echangesJ1, echangesJ2);
    }
    // Victoire après egalite
    else {
        // Le joueur qui a 2 echanges gagnants de plus que l'autre gagne le jeu (avantage + balle de match)
        if ((echangesJ1 - echangesJ2) == 2){
            AfficheScore(J1, echangesJ1, echangesJ2);
        }
        else if ((echangesJ2 - echangesJ1) == 2) {
            AfficheScore(J2, echangesJ1, echangesJ2);
        }
        else {
            cout << "Erreur Score : Impossible de déterminer un gagnant" << endl;
        } 
    }
}

void AfficheScore(string gagnant, int echangesJ1, int echangesJ2){
    cout << "Le " << gagnant << " joueur gagne : " << endl
    << "Score : Jeu - ";

    if (gagnant == J1){
        cout << getPoints(echangesJ2) << endl;
    }
    else {
        cout << getPoints(echangesJ1) << endl;
    }

}

string getPoints(int echanges){
    if (echanges >= 3){
        return "40";
    }

    switch (echanges)
    {
    case 0:
        return "0";
        break;
    case 1:
        return "15";
        break;
    case 2:
        return "30";
        break;
    default:
        return "Erreur score";
    }
}

int main(){
    DetermineScore(5,7);
}
